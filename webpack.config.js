const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require("webpack");
var isProd = process.env.NODE_ENV === 'production';//return true or false
var mode = process.env.NODE_ENV || 'development';
var cssDev = ['style-loader','css-loader','postcss-loader','sass-loader'];
var cssProd =  ExtractTextPlugin.extract({ //extract text d disable
    //    use : ['style-loader','css-loader','sass-loader'], //extract text d disable HMR ONLY
            fallback : 'style-loader',//HMR
            use : ['css-loader','sass-loader'],//HMR
            publicPath : '/dist/',//HMR
            allChunks: true,
        });
//const bootstrapEntryPoints = require('./webpack.bootstrap.config.js');

var cssConfig = isProd ? cssProd : cssDev;
//const bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;



module.exports = {
    devtool : (mode === 'development') ? 'inline-source-map' : false,
    mode : mode,
    entry : {
        app : './src/assets/js/app.js',
       // bootstrap : bootstrapConfig,
    },
    output : {
        path: path.resolve(__dirname, 'dist'),
        filename : './js/[name].bundle.js',
        //publicPath : '/dist/',//HMR

    },
    module : {
        rules : [
            {
                test : /\.scss$/,
                use : cssConfig
            }, 
            {
                test : /\.pug$/,
                use : ['raw-loader','pug-html-loader'],
            },
            { 
                test: /\.(jpe?g|gif|png|svg)$/i, 
                use:  [
                    //'file-loader?name=images/[name].[ext]',
                   'file-loader?name=[name].[ext]&outputPath=images/&publicPath=images/',
                    'image-webpack-loader'
                ]
            },
                        { 
                test: /\.(woff2?)$/, 
                use: 'url-loader?limit=10000&name=fonts/[name].[ext]' 
            },
            { 
                test: /\.(ttf|eot)$/, 
                use: 'file-loader?name=fonts/[name].[ext]'
            },
        ]
    },
    devServer: {
        contentBase: path.join( __dirname, 'dist'),
        compress: true,
        //port: 9201,
        stats: 'minimal',
        open:true,
        hot:false,
       // watchContentBase: true,


    },
    
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Tether: 'tether',
            'window.Tether': 'tether',
            Popper: ['popper.js', 'default'],
            Alert: 'exports-loader?Alert!bootstrap/js/dist/alert',
            Button: 'exports-loader?Button!bootstrap/js/dist/button',
            Carousel: 'exports-loader?Carousel!bootstrap/js/dist/carousel',
            Collapse: 'exports-loader?Collapse!bootstrap/js/dist/collapse',
            Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
            Modal: 'exports-loader?Modal!bootstrap/js/dist/modal',
            Popover: 'exports-loader?Popover!bootstrap/js/dist/popover',
            Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/dist/scrollspy',
            Tab: 'exports-loader?Tab!bootstrap/js/dist/tab',
            Tooltip: 'exports-loader?Tooltip!bootstrap/js/dist/tooltip',
            Util: 'exports-loader?Util!bootstrap/js/dist/util',
          }),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        minify : {
            collapseWhitespace : false,
        },
        hash : false,
        template: 'src/index.html'
      }), // Generates default index.html
   

      new ExtractTextPlugin({
          filename : 'css/[name].css',
          disable : !isProd,
          allChunks : true
      }),
      new webpack.HotModuleReplacementPlugin(),//HMR GLOBAL
      new webpack.NamedModulesPlugin(),//


    ]
}